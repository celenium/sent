# Scroll

- Check the (Readme)[https://codeberg.org/celenium/scroll/src/branch/master/README.md-by-suckless] by suckless, to know more.
- Patched to have dark background by default, and can be toggled back to white.
- Other patches applied at in the "patched-diffs" folder.
